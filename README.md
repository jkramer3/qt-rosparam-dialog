# Qt rosparam dialog

Qt simple dialog box for interacting with the ROS parameter server.

<p><img src="./qt-rosparam_screenshot.png" alt="Qt ROS Parameter Dialog" width="400" border="3"/></p>
This class presents a Qt dialog box that displays editable values
of the ROS parameter server. Entries are obtained via a call to
a node handle's `getParam` method; a depth-first search is
performed on the returned XmlRpc to fill the widget.

Although set up here as a standalone ROS package, it should be easy
enough for anyone using it to simply copy into the source code tree.
Actual usage is akin to any `QDialog`; simply declare an instance
with the base namespace and expanded arguments, then call the `exec()`
method:
```cpp
#include <qt-rosparam-dialog/rosparam_dialog.hpp>
...
rosparam::RosparamDialog rpd("/", true, this);
if (rpd.exec()) {
    // QDialog::Accepted returned, values already written
}
```

